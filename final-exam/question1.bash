#!/usr/bin/env bash

HEADER_COUNTER=0
function displayHeader() {
    HEADER_COUNTER=$((HEADER_COUNTER+1))
    echo
    echo '========================================'
    echo "${HEADER_COUNTER} => ${@}"
    echo
}

function echoerr() {
    echo 'ERROR:' $@ 1>&2
}

function sleeps() {
    seconds=$1
    echo -n "Sleeping for ${seconds} seconds "
    for i in $(seq 1 $seconds) ; do
        sleep 1
        if [[ "x$(( $i % 10))" == "x0" ]] ; then
            echo -n "${i}s"
        else
            echo -n '.'
        fi
    done
    echo
}

function cleanup() {
    echo 'Killing Processes:'
    pgrep --list-name -u $(whoami) mongod
    pkill -u $(whoami) mongod
    echo 'Cleaning up directories:'
    rm -rfv ${baseDir}
}

scriptName=$(basename ${BASH_SOURCE[0]})
baseDir=/tmp/m102-final-${scriptName}

rollbackScript="/mnt/files/courses/M102-mongodb-for-dbas/final_exam.3de1e6ec52d1/question_1/rollback/a.js"
if [[ ! -f ${rollbackScript} ]] ; then
    echoerr 'The expected rollback script does not exist:' ${rollbackScript}
    echoerr 'Cleaning up due to error.'
    exit
fi

displayHeader 'Cleaning up older runs of' ${scriptName}
cleanup

displayHeader 'Sleeping for a bit to ensure old processes are really dead.'
sleeps 10

dbPath1=${baseDir}/z1
dbPath2=${baseDir}/z2
dbPath3=${baseDir}/z3
mkdir -p ${dbPath1}
mkdir -p ${dbPath2}
mkdir -p ${dbPath3}

logBase=${baseDir}/logs
mkdir -p ${logBase}
dbLogPath1=${logBase}/z1.log
dbLogPath2=${logBase}/z2.log
dbLogPath3=${logBase}/z3.log

dbPort1=27001
dbPort2=27002
dbPort3=27003

bindIf=127.0.0.1
replSetName=z
commonArgs="--logappend --fork --smallfiles --oplogSize 50 --bind_ip ${bindIf} --replSet ${replSetName}"
displayHeader 'Starting a few servers to be a replicated cluster.'
mongod ${commonArgs} --logpath ${dbLogPath1} --port ${dbPort1} --dbpath ${dbPath1}
mongod ${commonArgs} --logpath ${dbLogPath2} --port ${dbPort2} --dbpath ${dbPath2}
mongod ${commonArgs} --logpath ${dbLogPath3} --port ${dbPort3} --dbpath ${dbPath3}

displayHeader 'Sleeping for a minute to ensure the replica set comes online.'
sleeps 60

displayHeader 'Initializing the replica set.'
mongo --shell ${bindIf}:${dbPort3} ${rollbackScript} <<EOF
    ourinit
    ourinit()
    exit
EOF

displayHeader 'Sleeping for a minute to ensure the replica set initializes.'
sleeps 60

displayHeader 'Determining who the PRIMARY is.'
isPrimary=$(mongo --port ${dbPort3} --quiet --eval "d=db.isMaster(); print(d['ismaster']);")
if [[ "x${isPrimary}" == "xtrue" ]] ; then
    echo "Mongo on ${dbPort3} is the primary."
else
    echoerr "Mongo on not ${dbPort3} is the primary, which was unexpected."
    echoerr 'Cleaning up due to error.'
    exit
fi

displayHeader 'Inserting some data.'
mongo --shell ${bindIf}:${dbPort3} ${rollbackScript} <<EOF
    db.foo.insert( { _id : 1 }, { writeConcern : { w : 2 } } )
    db.foo.insert( { _id : 2 }, { writeConcern : { w : 2 } } )
    db.foo.insert( { _id : 3 }, { writeConcern : { w : 2 } } )
    exit
EOF

displayHeader 'Shutting down the "extra son of a bitch"'
mongo --shell ${bindIf}:${dbPort3} ${rollbackScript} <<EOF
    var a = connect("${bindIf}:${dbPort1}/admin");
    a.shutdownServer()
    rs.status()
    exit
EOF

displayHeader 'Inserting some EXTRA data.'
mongo --shell ${bindIf}:${dbPort3} ${rollbackScript} <<EOF
    db.foo.insert( { _id : 4 } )
    db.foo.insert( { _id : 5 } )
    db.foo.insert( { _id : 6 } )
    exit
EOF

displayHeader 'Starting up the first "son of a bitch" again.'
mongod ${commonArgs} --logpath ${dbLogPath1} --port ${dbPort1} --dbpath ${dbPath1}

me=$(whoami)
expectedNumOfProcs=3
actualNumOfProcs=$(ps -A | pgrep -u ${me} mongod | wc -l)
displayHeader "Ensuring there are 3 mongod processes running"
if [[ "x${actualNumOfProcs}" == "x${expectedNumOfProcs}" ]] ; then
    echo "Expected number of mongod processes verified: " ${actualNumOfProcs}
else
    echoerr "Expected ${expectedNumOfProcs}-processes but found ${actualNumOfProcs}"
    echoerr 'Cleaning up due to error.'
    exit
fi

displayHeader 'Getting the answer to the question.'
mongo --shell ${bindIf}:${dbPort3} ${rollbackScript} <<EOF
    db.foo.find()
    print("!!ANSWER COMING!!")
    db.foo.count()
    print("!!END OF ANSWER!!")
    exit
EOF

displayHeader 'Cleaning up all of the damn resources.'
cleanup

